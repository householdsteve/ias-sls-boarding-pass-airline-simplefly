import React, { Component } from 'react'
import styles from './Admin.module.css'

import {
  getSession,
  deleteSession 
} from '../../utils'

import { 
  withRouter,
  Link,
} from 'react-router-dom'

export default class Admin extends Component {

 constructor(props) {
    super(props)
    this.state = {}

    // Bindings
    this.logout = this.logout.bind(this)
    console.log(props)
  }

  async componentDidMount() {

    const userSession = getSession()

    this.setState({
      session: userSession,
    })
  }

  /**
   * Log user out by clearing cookie and redirecting
   */
  logout() {
    deleteSession()
    this.props.history.push(`/`)
  }

  render() {
    return (
      <>
      <header className="navbar navbar-expand-sm navbar-dark sticky-top bg-primary flex-md-nowrap p-2 shadow">
  <a className="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="#">Intelisys Aviation</a>
  <button className="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="navbar-collapse ml-auto">
  <ul className="navbar-nav mr-auto text-white">
    <li className="nav-item">
      Digital Boarding Passes
    </li>

  </ul>
  <ul className="navbar-nav">
    <li className="nav-item">
      <span className="nav-link"> { this.state.session ? this.state.session.userEmail : '' }</span>
    </li>
    <li className="nav-item">
      <span className="nav-link" onClick={this.logout}>Sign out</span>
    </li>
  </ul>
  </div>
</header>

<div className="container-fluid h-100">
  <div className="row h-100">
    <nav id="sidebarMenu" className="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div className="position-sticky pt-3">
      <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Navigate</span>
        </h6>
        <ul className="nav flex-column">
          <li className="nav-item">
            <Link to="/" className="nav-link">
              Dashboard
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/generate" className="nav-link">
              Generate
            </Link>
          </li>
        </ul>

        <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Reports</span>
        </h6>
        <ul className="nav flex-column mb-2">
          <li className="nav-item">
            <a className="nav-link" href="#">
              <span data-feather="file-text"></span>
              Coming soon
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <main className="col-md-9 ml-sm-auto col-lg-10 px-md-4">
      <div className="container mt-5 border-bottom">
        {this.props.children}
      </div>
    </main>
  </div>
</div>
</>
    )
  }
  
}