import React, { Component } from 'react'
import {
  Link,
  withRouter
} from 'react-router-dom'
import styles from './Home.module.css'

class Home extends Component {

  constructor(props) {
    super(props)
    this.state = {}
  }

  async componentDidMount() { }

  render() {

    return (
      <div className={`animateFadeIn container`}>
        <div className={`row`}>
        <div className={`col-6 mx-auto text-center`}>
            <div className={`${styles.containerCta}`}>
              <Link to='/login' className={`${styles.linkSignIn}`}>sign-in</Link>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(Home)