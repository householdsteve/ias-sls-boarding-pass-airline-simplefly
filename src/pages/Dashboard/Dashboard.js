import React, { Component } from 'react'
import { 
  withRouter,
  Link,
} from 'react-router-dom'
import styles from './Dashboard.module.css'

import {
  searchPassByLocator,
} from '../../utils'

import { useForm } from "react-hook-form";

//import passData from "./passes.json";

const PassList = (props) => {

  return (
    <div className="mt-4">
      {props.passes.length < 1 && (
        <div className="alert alert-warning">No records have been created for this PNR</div>
      )}
      {props.passes.length > 0 && (
      <>
      <h3>Passes Linked to: <i>{props.passes.first.reservationLocator}</i></h3>
      <table className="table table-bordered">
      {/* add this class for mobile devices: table-responsive */}
        <thead>
          <tr>
            <th scope="col">Pass ID</th>
            <th scope="col">Edited Date</th>
            <th scope="col">Passenger</th>
            <th scope="col">Routing</th>
            <th scope="col">Pass</th>
          </tr>
        </thead>
        <tbody>
   
      {props.passes.map((pass,index) => {
      const bp = pass.iOSPassTemplates[0].boardingPass;
      const paxName = Object.keys(bp).reduce((acc,k) => {
        const needle = bp[k].length && typeof bp[k] === "object" && bp[k].find(item => item.key === "passenger-name") || null;
        if(needle) acc.push(needle);
        return acc;
      },[])[0]
      
      console.log(pass.iOSPassTemplates.first.boardingPass)
      return (<tr key={`p-${index}`} className="">
        <td scope="row" className=""><a href={pass.passLandingUrl} target="_blank">{pass.passId}</a></td>
        <td scope="row" className="">{pass.modifiedDate}</td>
        <td scope="row" className="">{paxName.value}</td>
        <td scope="row" className="">{pass.iOSPassTemplates.first.boardingPass.primaryFields.find(f => f.key === "origin").value}-{pass.iOSPassTemplates.last.boardingPass.primaryFields.find(f => f.key === "destination").value}</td>
        <td scope="row" className=""><a href={pass.passLandingUrl} target="_blank">View Pass</a></td>
      </tr>)})}
    </tbody>
    </table>
    </>
    )}
    </div>
  );
}

const SearchReservationForm = (props) => {
  const { register, handleSubmit, watch, errors, formState } = useForm();

  const { isDirty, isSubmitting } = formState;

  const onSubmit = async (data) => {
    //props.onLoadingChange();

    const passes = await searchPassByLocator(data.reservationLocator);
    props.returnPasses(passes);
   // props.onLoadingChange();
    console.log(passes);
  }

  //console.log(watch("paxCount")); // watch input value by passing the name of it

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
  <div className="form-group row">
    <label htmlFor="reservationLocator" className="col col-form-label">PNR (Reservation Locator)</label>
    <div className="col-sm-10">
      <input type="text" className="form-control" name="reservationLocator" id="reservationLocator" defaultValue="" ref={register} />
    </div>
  </div>

  <button type="submit" disabled={isSubmitting} className="btn btn-outline-light rounded-pill px-4">{(isSubmitting) ? 'Searhing...' : 'Search'}</button>
</form>
  );
}



class Dashboard extends Component {

  constructor(props) {
    super(props)
    this.state = {}
    //this.state.passes = passData
    this.state.passes = null

    // Bindings
    this.setPasses = this.setPasses.bind(this)
  }

  async componentDidMount() {}

  setPasses(data) {
    this.setState({
      passes: data
    });
  }

  render() {

    return (
      <>
      <div className={`animateFadeIn row h-100 mb-5`}>
        <div className={`animateFadeIn col-12 col-sm-6 h-100`}>
          <div className="card h-100 text-white bg-secondary mb-3">
            <div className="card-body">
              <h1 className="display-2">213</h1>
              <p className="card-text">Passes generated so far.</p>
            </div>
          </div>
        </div>
        <div className={`animateFadeIn col-12 col-sm-6 h-100`}>
          <div className="card h-100 text-white bg-info mb-3">
            <div className="card-body">
              <h1 className="display-2">10:38am</h1>
              <p className="card-text">Last generated pass: MTiqMsD-x3D-S1</p>
            </div>
          </div>
        </div>
        
      </div>
      <div className={`animateFadeIn row h-100`}>
        <div className={`animateFadeIn col-12 col-sm-4 h-100`}>
            <h3 className="">Generate Pass</h3>
          <div className="card h-100 bg-light mb-3">
            <div className="card-body">
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Reservation Locator</label>
              <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
            </div>
            <button type="submit" className="btn btn-primary rounded-pill px-4">Start</button>
            </div>
          </div>
        </div>
        <div className={`animateFadeIn col-12 col-sm-4 h-100`}>
          <div className="card h-100 bg-primary text-white mb-3">
            <div className="card-header">Find Existing Pass</div>
            <div className="card-body">
            <SearchReservationForm returnPasses={this.setPasses} />
            </div>
          </div>
        </div>
        <div className={`animateFadeIn col-12 col-sm-4 h-100`}>
        <div className="card h-100 border-info mb-3">
          <div className="card-header">Service Status</div>
          <div className="card-body text-info">
            <img src="https://uptimerobot.com/assets/images/status-page.svg" className="img-fluid" />
          </div>
        </div>
        </div>
      </div>
      
      {(this.state.passes) && (<PassList passes={this.state.passes.pass} />)}
      </>
    )
  }
}

export default withRouter(Dashboard)