import React, { Component } from 'react'
import { 
  withRouter 
} from 'react-router-dom'
import styles from './Generate.module.css'
import Loading from '../../fragments/Loading'
import {
  getSession,
  deleteSession,
  createReservation,
  getReservation,
  generateBoardingPass,
} from '../../utils'

import { useForm } from "react-hook-form";

//import reservationData from './example.json';
//import reservationData from './departures-example.json';

const BookingForm = (props) => {
  const { register, handleSubmit, watch, errors, formState } = useForm();

  const { isDirty, isSubmitting } = formState;

  const onSubmit = async (data) => {
    props.onLoadingChange();
    
    let payload = {
      "daysToSearch": 7,
      "airline": "intelisys",
      "apiKey": "V0lER0VUOjFxYXp4c3cy",
      "ameliaEnv" : "",
      "paxCounts": {
        "adult": 1
      }, 
      ...data
    }
    payload.paxCounts.adult = parseInt(data.paxCount);
    payload.travelOptionIndex = parseInt(data.travelOptionIndex);
    console.log(payload)

    const reservation = await createReservation(payload);
    const reservationByDepartures = await getReservation({
        "reservationLocator": reservation.reservation.locator,
        "airline": "intelisys",
        "apiKey": "V0lER0VUOjFxYXp4c3cy",
        "ameliaEnv" : "",
      });
    props.returnReservation(reservationByDepartures);
    props.onLoadingChange();
    console.log(reservationByDepartures);
  }

  //console.log(watch("paxCount")); // watch input value by passing the name of it

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
  <div className="form-group row">
    <label htmlFor="inputEmail3" className="col-sm-10 col-form-label">Email to receive Itinerary</label>
    <div className="col-sm-10">
      <input type="email" className="form-control" name="itineraryEmail" id="inputEmail3" placeholder="Email" defaultValue="smccrumb@intelisysaviation.com" ref={register} />
    </div>
  </div>
  <div className="form-group row">
    <label htmlFor="inputPassword3" className="col-sm-10 col-form-label">CityPair</label>
    <div className="col-sm-10">
      <input type="text" className="form-control" name="cityPair" id="inputPassword3" placeholder="" defaultValue="YUL-YQM" ref={register} />
    </div>
  </div>
  <div className="form-group row">
    <label htmlFor="inputPassword4" className="col-sm-10 col-form-label">Travel Option Index</label>
    <div className="col-sm-10">
      <input type="number" className="form-control" name="travelOptionIndex" id="inputPassword4" placeholder="" defaultValue="0" ref={register} />
    </div>
  </div>
  <div className="form-group row">
    <label htmlFor="exampleFormControlSelect1" className="col-sm-10 col-form-label">Adult passengers</label>
    <div className="col-sm-10">
      <select className="form-control" name="paxCount" id="exampleFormControlSelect1" ref={register}>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
        <option>6</option>
        <option>7</option>
        <option>8</option>
        <option>9</option>
      </select>
    </div>
  </div>
  <fieldset className="form-group">
    <div className="row">
      <legend className="col-form-label col-sm-10 pt-0">Trip type</legend>
      <div className="col-sm-10">
        <div className="form-check">
          <input className="form-check-input" type="radio" name="bookRoundtrip" id="gridRadios1" value="yes" defaultChecked ref={register} />
          <label className="form-check-label" htmlFor="gridRadios1">
            Round trip
          </label>
        </div>
        <div className="form-check">
          <input className="form-check-input" type="radio" name="bookRoundtrip" id="gridRadios2" value="no" ref={register} />
          <label className="form-check-label" htmlFor="gridRadios2">
            One way
          </label>
        </div>
      </div>
    </div>
  </fieldset>
  <div className="form-group row">
    <div className="col-sm-10">
      <button type="submit" disabled={isSubmitting} className="btn btn-primary">Create Booking</button>
    </div>
  </div>
</form>
  );
}

const SearchReservationForm = (props) => {
  const { register, handleSubmit, watch, errors, formState } = useForm();

  const { isDirty, isSubmitting } = formState;

  const onSubmit = async (data) => {
    props.onLoadingChange();
    
    let payload = {
      "reservationLocator": data.reservationLocator,
      "airline": "intelisys",
      "apiKey": "V0lER0VUOjFxYXp4c3cy",
      "ameliaEnv" : "",
    }

    const reservation = await getReservation(payload);
    props.returnReservation(reservation);
    props.onLoadingChange();
    console.log(reservation);
  }

  //console.log(watch("paxCount")); // watch input value by passing the name of it

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
  <div className="form-group row">
    <label htmlFor="inputEmail3" className="col-sm-10 col-form-label">PNR (Reservation Locator)</label>
    <div className="col-sm-10">
      <input type="text" className="form-control" name="reservationLocator" id="inputEmail3" defaultValue="" ref={register} />
    </div>
  </div>
  <div className="form-group row">
    <div className="col-sm-10">
      <button type="submit" disabled={isSubmitting} className="btn btn-primary">Get Reservation</button>
    </div>
  </div>
</form>
  );
}


const BoardingPassForm = (props) => {
  const { register, handleSubmit, watch, errors, formState } = useForm();

  const { isDirty, isSubmitting } = formState;

  const onSubmit = async (data) => {
    props.onLoadingChange();

    const paxSelection = data.passenger.split("-");
    let payload = {
      "reservationLocator": data.pnr,
      "journeyIndex": paxSelection[0],
      "paxIndex": paxSelection[1],
      "airline": "intelisys",
      "apiKey": "V0lER0VUOjFxYXp4c3cy",
      "ameliaEnv": ""
    }

    console.log(payload)

    const boardingPass = await generateBoardingPass(payload)
    if(!Object.keys(boardingPass).length) alert("Flight not open for check-in")
    console.log(boardingPass.pass)
    props.returnPass(boardingPass.pass)
    props.onLoadingChange();
    
  }

  //console.log(errors); // watch input value by passing the name of it

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
    <h4 className="text-secondary">PNR: {props.reservation.locator}</h4>
    {props.reservation.journeys.map((journey, index) => 
              <div key={`j-${index}`} className="journey row mb-4">
                <div className="col">
                  <h2>{journey.segments.first.departure.airport.code}-{journey.segments.last.arrival.airport.code}</h2>
                  <small>{journey.segments.first.departure.localScheduledTime}</small>
                    {journey.passengerJourneyDetails.map((passengerJourneyDetail, i) => {
                        const pax = props.reservation.passengers.find(p => p.key === passengerJourneyDetail.passenger.key);
                        return (<div key={`p-${i}-${index}`} className="journey row">
                          <div className="col">
                            <label className="form-check-label py-2">
                            <input className="form-check-input" type="radio" name="passenger" value={`${index}-${i}`} ref={register({required: true})} /> 
                            {pax.reservationProfile.firstName} {pax.reservationProfile.lastName}
                            </label>
                          </div>
                        </div>)
                    })}
                </div>
              </div>
            )}
            { errors.passenger && (
                                <div role="alert" className="alert alert-warning">
                                  You must select a passenger
                                </div>
                              )
                            }
            <input type="hidden" defaultValue={props.reservation.locator} ref={register} name="pnr" />
      <div className="form-group row">
        <div className="col-sm-10">
          <button type="submit" disabled={isSubmitting} className="btn btn-primary">Generate Boarding Pass</button>
        </div>
      </div>
    </form>
  );
}
const BoardingPassFormByDepartures = (props) => {
  const { register, handleSubmit, watch, errors, formState } = useForm();

  const { isDirty, isSubmitting } = formState;

  const onSubmit = async (data) => {
    props.onLoadingChange();

    const paxSelection = data.passenger.split("-");
    let payload = {
      "reservationLocator": data.pnr,
      "journeyIndex": parseInt(paxSelection[0]),
      "paxIndex": parseInt(paxSelection[1]),
      "airline": "intelisys",
      "apiKey": "V0lER0VUOjFxYXp4c3cy",
      "ameliaEnv": ""
    }

    console.log(payload)

    const boardingPass = await generateBoardingPass(payload)
    if(!Object.keys(boardingPass).length) alert("Flight not open for check-in")
    console.log(boardingPass.pass)
    props.returnPass(boardingPass.pass)
    props.onLoadingChange();
    
  }

  //console.log(errors); // watch input value by passing the name of it

  return (
    <>
    {props.reservation.locator && (<form onSubmit={handleSubmit(onSubmit)}>
    <h4 className="text-secondary">PNR: {props.reservation.locator}</h4>
    {props.reservation.journeys.map((journey, index) => {
             return (<div key={`j-${index}`} className="journey row mb-4">
                <div className="col">
                  <h2 className="mb-0">{journey.journey.departure}-{journey.journey.arrival}</h2>
                  <small>Departure: {journey.journey.departureTime}</small>
                  <table className="table table-bordered">
                    {/* add this class for mobile devices: table-responsive */}
                    <thead>
                      <tr>
                        <th scope="col">Segments</th>
                        <th scope="col">Journeys</th>
                      </tr>
                    </thead>
                    <tbody>
   
                    <tr>
                      <td scope="row">{journey.segments.map(l => `${l.departure}-${l.arrival}`).join(" → ")}</td>
                      <td>{journey.legsAvailableForBoardingPass.map(l => `${l.departure}-${l.arrival}`).join(" → ")}</td>
                    </tr>
                    </tbody>
                  </table>
                  
                  <div className="list-group">
                  {!journey.legsAvailableForBoardingPass.length && (
                    <div className="alert alert-warning">Not available for pass generation</div>
                  )}

                  {journey.legsAvailableForBoardingPass.length > 0 && (
                    <h6 className="">Select Passenger</h6>
                  )}
                  
                  {journey.passengers.map((pax, i) => {
                        return (<div key={`p-${i}-${index}`} className="pax list-group-item">
                          <div className="">
                            <label className="form-check-label py-2 d-flex justify-content-between pointer">
                            <span>{pax.firstName} {pax.lastName}</span>
                            <input className="" type="radio" name="passenger" value={`${journey.generatable}-${i}`} ref={register({required: true})} /> 
                            </label>
                          </div>
                        </div>)
                    })}
                  </div>
                </div>
              </div>
            )})}
            { errors.passenger && (
                                <div role="alert" className="alert alert-warning">
                                  You must select a passenger
                                </div>
                              )
                            }
            <input type="hidden" defaultValue={props.reservation.locator} ref={register} name="pnr" />
      <div className="form-group row">
        <div className="col-sm-10">
          <button type="submit" disabled={isSubmitting} className="btn btn-primary">Generate Boarding Pass</button>
        </div>
      </div>
    </form>)}
    {!props.reservation.locator && (
      <div className="alert alert-warning">No journeys available for pass generation</div>
    )}
    </>
  );
}

class Generate extends Component {

  constructor(props) {
    super(props)
    this.state = {}
    this.state.loading = true
    this.state.reservation = null
    //this.state.reservation = reservationData
    this.state.pass = null
    this.state.searchByPnr = true

    // Bindings
    this.updateLoading = this.updateLoading.bind(this)
    this.setReservation = this.setReservation.bind(this)
    this.setPass = this.setPass.bind(this)
  }

  async componentDidMount() {
    const userSession = getSession()

    this.setState({
      session: userSession,
      loading: false
    })

  }

  updateLoading() {
    this.setState({
      loading: !this.state.loading
    });
  }
  
  setReservation(data) {
    this.setState({
      reservation: data
    });
  }

  setPass(data) {
    this.setState({
      pass: data
    });
  }

  render() {

    return (
      <div className={`container mt-5 animateFadeIn`}>
      
        <div className={`row`}>
          <div className={`col-6`}>
            <SearchReservationForm onLoadingChange={this.updateLoading} returnReservation={this.setReservation} />

            <h4 className="text-muted mt-5 mb-3">OR for demo purposes only:</h4>
            <h5 className="text-primary">Make a booking</h5>
            <BookingForm onLoadingChange={this.updateLoading} returnReservation={this.setReservation} />

          </div>
          <div className={`col-6`}>
          
          {(this.state.pass) && (
              <div className="animateFadeIn card shadow mt-2 mb-4">
                <div className="card-header">
                  <h5 className="m-0 p-0">Generated pass</h5>
                </div>
                <div className="card-body">
                  <a href={this.state.pass.passLandingUrl} target="_blank">{this.state.pass.passLandingUrl}</a>
                </div>
                <div className="card-footer border-0 bg-white">
                  <small className="">Actions for Customer</small>
                  <div className="d-flex">
                    <button className="btn btn-outline-info mr-2 d-inline-block">SMS</button>
                    <button className="btn btn-outline-info mr-2 d-inline-block">E-mail</button>
                  </div>
                </div>
              </div>
            )}

            {(this.state.reservation && this.state.searchByPnr) && (<BoardingPassFormByDepartures reservation={this.state.reservation.reservation} returnPass={this.setPass} onLoadingChange={this.updateLoading} />)}
            {(this.state.reservation && !this.state.searchByPnr) && (<BoardingPassForm reservation={this.state.reservation.reservation} returnPass={this.setPass} onLoadingChange={this.updateLoading} />)}
            
          </div>
        </div>
        {this.state.loading && (
            <div className={styles.loader}>
              {< Loading className={styles.containerLoading} />}
            </div>
          )}
      </div>
    )
  }
}

export default withRouter(Generate)